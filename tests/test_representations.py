#!/usr/bin/env python3
from unittest import TestCase
from morphalign.representations import morphological_segmentation, from_alignment_codes, \
    to_alignment_codes
import numpy as np

# -*- coding: utf-8 -*-
class TestRepresentations(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.forms = [[("m", "ɛ", "l"), ("m", "e", "l", "e"),
                      ("m", "ɛ", "l", "ø", "r", "e")],
                     [("f", "i", "n", "i"), ("f", "i", "n", "i", "s"),
                      ("f", "i", "n", "i", "s", "e")]]
        cls.alignments = [[["m", "ɛ", "l", "-", "-", "-"],
                           ["m", "e", "l", "-", "-", "e"],
                           ["m", "ɛ", "l", "ø", "r", "e"]],

                          [["f", "i", "n", "i", "-", "-"],
                           ["f", "i", "n", "i", "s", "-"],
                           ["f", "i", "n", "i", "s", "e"]
                          ]]
        cls.codes = [[[0, 1, 2], [0, 1, 2, 5], [0, 1, 2, 3, 4, 5]],
                     [[0, 1, 2, 3], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4, 5]]]

    def test_alignment_codes_chained(self):
        for aligned, paradigm in zip(self.alignments, self.forms):
            codes = [[int(i) for i in row.split(" ")]
                     for row in to_alignment_codes(np.array(aligned))]
            result = from_alignment_codes(codes,paradigm)
            self.assertSequenceEqual(result.tolist(), aligned)

    def test_to_alignment_codes(self):
        codes = [to_alignment_codes(a) for a in self.alignments]
        for codes, expected in zip(codes, self.codes):
            codes = [[int(i) for i in row.split(" ")] for row in codes]
            self.assertSequenceEqual(codes, expected)

    def test_from_alignment_codes(self):
        alignments2 = [from_alignment_codes(c,f) for c,f in zip(self.codes,self.forms)]
        for result, expected in zip(alignments2, self.alignments):
            self.assertSequenceEqual(result.tolist(), expected)

    def test_morphological_segmentation(self):
        stem, exponents, markers = morphological_segmentation(self.alignments[0])
        expected = ('m - l -',
                    ['- ɛ -', '- e - e', '- ɛ - ø r e'],
                    [['ɛ'], ['e', 'e'], ['ɛ', 'ø r e']])
        self.assertEqual(stem, expected[0])
        self.assertSequenceEqual(exponents, expected[1])
        self.assertSequenceEqual(markers, expected[2])

        stem, exponents, markers = morphological_segmentation(self.alignments[1])
        expected = ('f i n i -',
                    ['-', '- s', '- s e'],
                    [[], ['s'], ['s e']])
        self.assertEqual(stem, expected[0])
        self.assertSequenceEqual(exponents, expected[1])
        self.assertSequenceEqual(markers, expected[2])
