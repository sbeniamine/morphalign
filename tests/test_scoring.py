#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from unittest import TestCase
from morphalign import scoring
from morphalign.align_pairwise import columns_to_profiles
from inventory.sounds import SoundInventory
import pandas as pd
import numpy as np
import io


class TestScoring(TestCase):

    def __init__(self, *args, **kwargs):
        super(TestScoring, self).__init__(*args, **kwargs)
        self.alphabet_l = 13
        self.simple_scores = np.array([[0, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 1],
                                       [20, 0, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 1],
                                       [20, 20, 0, 20, 20, 20, 20, 20, 20, 20, 20, 20, 1],
                                       [20, 20, 20, 0, 20, 20, 20, 20, 20, 20, 20, 20, 1],
                                       [20, 20, 20, 20, 0, 20, 20, 20, 20, 20, 20, 20, 1],
                                       [20, 20, 20, 20, 20, 0, 20, 20, 20, 20, 20, 20, 1],
                                       [20, 20, 20, 20, 20, 20, 0, 20, 20, 20, 20, 20, 1],
                                       [20, 20, 20, 20, 20, 20, 20, 0, 20, 20, 20, 20, 1],
                                       [20, 20, 20, 20, 20, 20, 20, 20, 0, 20, 20, 20, 1],
                                       [20, 20, 20, 20, 20, 20, 20, 20, 20, 0, 20, 20, 1],
                                       [20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 0, 20, 1],
                                       [20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 0, 1],
                                       [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                                       ])
        self.seq_1 = [(1, 1, 2), (9, 2, 9), (12, 12, 1), (6, 6, 6), (12, 6, 12),
                      (7, 7, 7)]
        self.seq_2 = [(1,), (2,), (10,), (6,)]
        self.seq_3 = [(1, 1), (2, 2), (1, 1), (6, 12), (12, 7)]


    def test_sum_of_pairs(self):
        # with a list of tuples
        score = scoring.sum_of_pairs(self.seq_1, self.simple_scores)
        self.assertEqual(score, 86)

        # with a np array
        score = scoring.sum_of_pairs(np.array(tuple(self.seq_1)), self.simple_scores)
        self.assertEqual(score, 86)


    def test_profile_score_shallow(self):
        expected_scores =  [40, 80, 4, 3, 25]
        scores = []
        for a, b in zip(self.seq_1, self.seq_3):
            scores.append(scoring.scorer_shallow(a, b, self.simple_scores))
        self.assertSequenceEqual(scores, expected_scores)


    #
    def test_profile_score_deep(self):
        expected_scores = [40, 80, 4, 3, 25]
        scores = []
        pa = columns_to_profiles([self.seq_1], self.alphabet_l)[0]
        pb = columns_to_profiles([self.seq_3], self.alphabet_l)[0]
        for a, b in zip(pa, pb):
            scores.append(scoring.scorer_deep(a, b, self.simple_scores))
        self.assertSequenceEqual(scores, expected_scores)


    def test_edit_matrix(self):
        m = scoring.edit_matrix(self.alphabet_l)
        self.assertTrue((m == self.simple_scores).all())


    def test_feature_sim_matrix(self):
        df = pd.DataFrame([['segmental', 1, np.nan, 1, 0],
                           ['segmental', 1, np.nan, 0, 1],
                           ['segmental', 0, 1, 0, np.nan]],
                          index=['a', 'b', 'c'],
                          columns=['tier', 'cons', 'x', 'y', 'z'])

        alphabet = ['a', 'b', 'c', '-']
        alpha2int = {'a': 0, 'b': 1, 'c': 2, '-': 3}

        features = SoundInventory(alphabet, io.StringIO(df.to_csv(index=True)))
        sim_matrix = scoring.sim_matrix(features, alphabet, alpha2int, method="features")
        expected = np.array([[0., 0.66666667, 1., 0.33333333],
                             [0.66666667, 0., 0.75, 0.33333333],
                             [1., 0.75, 0., 0.33333333],
                             [0.33333333, 0.33333333, 0.33333333, 0.33333333]])
        self.assertTrue(
            sim_matrix.shape == expected.shape and np.allclose(sim_matrix, expected))


    def test_init_alphabet(self):
        letters = list("abcdefghijklmnopqrstuvwxyz")
        alphabet, alpha2int = scoring.init_alphabet(letters)
        self.assertEqual(len(alphabet), 27)
        for a in alphabet:
            self.assertEqual(a, alphabet[alpha2int[a]])


    def test_frisch_phon_sim_matrix(self):
        df = pd.DataFrame([['segmental', '1', None, '1', '0'],
                           ['segmental', '1', None, '0', '1'],
                           ['segmental', '0', '1.0', '0', None]], index=['a', 'b', 'c'],
                          columns=['tier', 'cons', 'x', 'y', 'z'])
        alphabet = ['a', 'b', 'c', '-']
        alpha2int = {'a': 0, 'b': 1, 'c': 2, '-': 3}

        features = SoundInventory(alphabet, io.StringIO(df.to_csv(index=True)))
        sim_matrix = scoring.sim_matrix(features, alphabet, alpha2int, method="frisch")
        expected = np.array([[0., 0.71428571, 0.85714286, 0.35714286],
                             [0.71428571, 0., 0.71428571, 0.35714286],
                             [0.85714286, 0.71428571, 0., 0.35714286],
                             [0.35714286, 0.35714286, 0.35714286, 0.35714286]])
        self.assertTrue(
            sim_matrix.shape == expected.shape and np.allclose(sim_matrix, expected))
