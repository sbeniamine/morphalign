#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from unittest import TestCase
import numpy as np
from morphalign import utils


class TestUtils(TestCase):
    def test_ascii_tree(self):
        ## Basic tree, with variable length leaves
        labels = ("a", "b", "c", "long-leaf", "e")
        tree = np.array([[1, 2, .3, 2],
                         [0, 5, .8, 3],
                         [3, 4, 2, 2],
                         [6, 7, 2.5, 5],
                         ], dtype=np.double)
        expected = "\n".join(['        a ────┐─────────┐',
                              '        b ─┐──┘         │',
                              '        c ─┘            │',
                              'long-leaf ───────────┐──┘',
                              '        e ───────────┘   '])
        computed = utils.ascii_tree(tree, labels)
        self.assertEqual(expected, computed)

        ### More complex tree, with several branches of same length
        labels = list("abcdef")
        tree = np.array([[1, 2, .2, 2],  # 6
                         [6, 3, .2, 3],  # 7
                         [0, 7, .3, 4],  # 8
                         [4, 5, .5, 2],  # 9
                         [8, 9, 1, 6],
                         ], dtype=np.double)
        expected = "\n".join(['a ─────┐─────────────┐',
                              'b ───┐─┘             │',
                              'c ───┘               │',
                              'd ───┘               │',
                              'e ─────────┐─────────┘',
                              'f ─────────┘          '])  # 5
        computed = utils.ascii_tree(tree, labels)
        self.assertEqual(expected, computed)
