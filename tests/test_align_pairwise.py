#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from unittest import TestCase
from morphalign.align_pairwise import align_pairwise_deep, \
    align_pairwise_shallow, columns_to_profiles, _multibacktrack_iter, scorer_shallow, _align_pairwise, \
    align_pairwise_flat, _backtrack_iter, suffix_bias, infix_bias, prefix_bias
from morphalign.scoring import edit_matrix
import numpy as np


class TestAlignPairwise(TestCase):

    def __init__(self, *args, **kwargs):
        super(TestAlignPairwise, self).__init__(*args, **kwargs)
        self.alphabet_l = 33
        self.g = self.alphabet_l - 1
        self.simple_scores = edit_matrix(self.alphabet_l)
        self.ex_seqs = [((8,), (0,), (2,), (7,), (3,), (1,), (4,), (9,)),
                        ((8,), (0,), (2,), (7,), (3,), (1,), (6,), (3,), (5,))]

    def test_infix_bias(self):
        """ No bias produces an array length n between -max and 0"""
        self.assertListEqual([0], list(infix_bias(1)))
        self.assertListEqual([0, 0], list(infix_bias(2)))
        m = 1

        # Test that default max has not changed
        b1 = infix_bias(5)
        b2 = infix_bias(5, max=m)
        self.assertTrue(np.allclose(b1, b2))

        # i numbers ascending then descending in a triangle
        # odd numbers
        for i in range(3, 13, 2):
            b = infix_bias(i, max=m)
            mid = i // 2
            self.assertTrue(b.shape[0] == i)
            self.assertTrue(b[mid] == 0)
            self.assertTrue(b[0] == m)
            diffs = np.diff(b)
            self.assertTrue((diffs[:mid] < 0).all())
            self.assertTrue((diffs[mid:] > 0).all())

        # even numbers
        for i in range(4, 14, 2):
            b = infix_bias(i)
            mid = i // 2
            self.assertTrue(b.shape[0] == i)
            self.assertTrue(b[mid] == 0)
            self.assertTrue(b[mid - 1] == 0)
            self.assertTrue(b[0] == m)
            diffs = np.diff(b)
            self.assertTrue((diffs[:mid - 1] < 0).all())
            self.assertTrue((diffs[mid:] > 0).all())

    def test_suffix_bias(self):
        """ No bias produces an array length n between -max and 0"""
        self.assertListEqual([0], list(suffix_bias(1)))  #
        m = 1

        # Test that default max has not changed
        b1 = suffix_bias(5)
        b2 = suffix_bias(5, max=m)
        self.assertTrue(np.allclose(b1, b2))

        # i numbers are between 0 and -max ascending
        for i in range(2, 10):
            b = suffix_bias(i)
            self.assertTrue(b.shape[0] == i)
            self.assertTrue(b[-1] == 0)
            self.assertTrue(b[0] == m)
            self.assertTrue((np.diff(b) < 0).all())

    def test_prefix_bias(self):
        """ No bias produces an array length n between 0 and -max"""
        self.assertListEqual([0], list(prefix_bias(1)))  #
        m = 1

        # Test that default max has not changed
        b1 = prefix_bias(5)
        b2 = prefix_bias(5, max=m)
        self.assertTrue(np.allclose(b1, b2))

        # i numbers are between 0 and -max descending
        for i in range(2, 10):
            b = prefix_bias(i, max=m)
            self.assertTrue(b.shape[0] == i)
            self.assertTrue(b[0] == 0)
            self.assertTrue(b[-1] == m)
            self.assertTrue((np.diff(b) > 0).all())

    def test_align_pairwise_deep_profiles(self):
        np.set_printoptions(threshold=100000000000)
        s1 = self.ex_seqs[0]
        s2 = self.ex_seqs[1]
        profile_left = columns_to_profiles([s1], self.alphabet_l)[0]
        profile_right = columns_to_profiles([s2], self.alphabet_l)[0]
        score, aligned = align_pairwise_deep(s1, s2, profile_left, profile_right, self.simple_scores)
        expected = [[(8, 8), (0, 0), (2, 2), (7, 7), (3, 3), (1, 1), (4, 32), (9, 32), (32, 6), (32, 3), (32, 5)]]
        self.assertAlmostEqual(score, 5)
        self.assertEqual(len(expected), len(aligned))
        for alignment in aligned:
            self.assertIn(alignment, expected)

    def test_align_pairwise_flat(self):
        np.set_printoptions(threshold=100000000000)
        s1 = self.ex_seqs[0]
        s2 = self.ex_seqs[1]
        score, aligned = align_pairwise_flat(s1, s2, self.simple_scores)
        expected = [[(8, 8), (0, 0), (2, 2), (7, 7), (3, 3), (1, 1), (4, 32), (9, 32), (32, 6), (32, 3), (32, 5)]]

        self.assertAlmostEqual(score, 5)
        self.assertEqual(len(expected), len(aligned))
        for alignment in aligned:
            self.assertIn(alignment, expected)
        #
        # s1 = ((6,), (3,), (23,), (30,))
        # s2 = ((11,), (16,), (5,), (24,), (6,), (3,), (23,), (30,))
        # res = [(6, 6), (3, 3), (23, 23), (30, 30)]
        # score, aligned = align_pairwise_flat(s1, s2, self.simple_scores, ambiguity=False)
        # #print(aligned)

    def test_align_pairwise_shallow_profiles(self):
        s1 = self.ex_seqs[0]
        s2 = self.ex_seqs[1]
        score, aligned = align_pairwise_shallow(s1, s2, self.simple_scores)
        expected = [[(8, 8), (0, 0), (2, 2), (7, 7), (3, 3), (1, 1), (4, 32), (9, 32), (32, 6), (32, 3), (32, 5)]]
        self.assertAlmostEqual(score, 5)
        self.assertEqual(len(expected), len(aligned))
        for alignment in aligned:
            self.assertIn(alignment, expected)

    def test__backtrack_iter(self):
        # Simple alignment
        ba = [(0,), (1,)]
        bobo = [(0,), (2,), (0,), (2,)]
        baba = [(0,), (1,), (0,), (1,)]
        batote = [(0,), (1,), (3,), (2,), (3,), (4,)]
        bakibu = [(0,), (1,), (6,), (7,), (0,), (7,)]
        bababobo = [(0, 0), (1, 2), (0, 0), (1, 2)]
        gap = self.g

        paths = _align_pairwise(bobo, baba, self.simple_scores, [gap], [gap], scorer_shallow)
        expected = [(0, 0), (2, 32), (32, 1), (0, 0), (2, 32), (32, 1)]  # ba-ba- / b-ob-o
        aligned = _backtrack_iter(paths, bobo, baba, gap)


        self.assertSequenceEqual(aligned, expected)

        paths = _align_pairwise(batote, bakibu, self.simple_scores, [gap], [gap], scorer_shallow)
        expected = [(0, 0), (1, 1), (3, 32), (2, 32), (3, 32), (4, 32), (32, 6), (32, 7), (32, 0), (32, 7)]
        aligned = _backtrack_iter(paths, batote, bakibu, gap)
        self.assertSequenceEqual(aligned, expected)

        paths = _align_pairwise(ba, baba, self.simple_scores, [gap], [gap], scorer_shallow)
        aligned = _backtrack_iter(paths, ba, baba, gap)
        expected = [(0, 0), (1, 1), (gap, 0), (gap, 1)]
        self.assertSequenceEqual(aligned, expected)

        paths = _align_pairwise(ba, bababobo, self.simple_scores, [gap], [gap], scorer_shallow)
        aligned = _backtrack_iter(paths, ba, bababobo, gap)
        expected = [(0, 0, 0), (1, 32, 32), (32, 1, 2), (32, 0, 0), (32, 1, 2)]
        self.assertSequenceEqual(aligned, expected)

        s1 = ((6,), (3,), (23,), (30,))
        s2 = ((11,), (2,), (0,), (31,), (6,), (3,), (24,), (30,))
        paths = _align_pairwise(s1, s2, self.simple_scores, [gap], [gap], scorer_shallow)
        aligned = _backtrack_iter(paths, s1, s2, gap)
        expected = [(32, 11), (32, 2), (32, 0), (32, 31), (6, 6), (3, 3), (23, 32), (32, 24), (30, 30)]
        self.assertSequenceEqual(aligned, expected)

    def test__multibacktrack_iter(self):
        # Simple alignment
        ba = [(0,), (1,)]
        bobo = [(0,), (2,), (0,), (2,)]
        baba = [(0,), (1,), (0,), (1,)]
        batote = [(0,), (1,), (3,), (2,), (3,), (4,)]
        bakibu = [(0,), (1,), (6,), (7,), (0,), (7,)]
        bababobo = [(0, 0), (1, 2), (0, 0), (1, 2)]
        gap = self.alphabet_l - 1

        paths = _align_pairwise(bobo, baba, self.simple_scores, [gap], [gap], scorer_shallow)
        expected = [[(0, 0), (2, 32), (32, 1), (0, 0), (2, 32), (32, 1)]]  # ba-ba- / b-ob-o
        aligned = _multibacktrack_iter(paths, bobo, baba, gap)
        self.assertSequenceEqual(aligned, expected,
                                 "Should generate only the ambiguous alignments which differ by their insertions.")

        paths = _align_pairwise(batote, bakibu, self.simple_scores, [gap], [gap], scorer_shallow)
        expected = [[(0, 0), (1, 1), (3, 32), (2, 32), (3, 32), (4, 32), (32, 6), (32, 7), (32, 0), (32, 7)]]
        aligned = _multibacktrack_iter(paths, batote, bakibu, gap)
        self.assertEqual(len(expected), len(aligned))
        for alignment in aligned:
            self.assertIn(alignment, expected)

        # Align with ambiguities: finds all ambiguities
        paths = _align_pairwise(ba, baba, self.simple_scores, [gap], [gap], scorer_shallow)
        aligned = _multibacktrack_iter(paths, ba, baba, gap)
        expected = [[(gap, 0), (gap, 1), (0, 0), (1, 1)],  # --ba / baba
                    [(0, 0), (gap, 1), (gap, 0), (1, 1)],  # b--a / baba
                    [(0, 0), (1, 1), (gap, 0), (gap, 1)]]  # ba-- / baba
        self.assertEqual(len(expected), len(aligned))
        for alignment in aligned:
            self.assertIn(alignment, expected)

        # # Align with ambiguities: finds all ambiguities... also in deeper profile
        paths = _align_pairwise(ba, bababobo, self.simple_scores, [gap], [gap], scorer_shallow)
        aligned = _multibacktrack_iter(paths, ba, bababobo, gap)
        expected = [[(0, 0, 0), (1, 32, 32), (32, 1, 2), (32, 0, 0), (32, 1, 2)],
                    [(32, 0, 0), (32, 1, 2), (0, 0, 0), (1, 32, 32), (32, 1, 2)]]
        self.assertEqual(len(expected), len(aligned))
        for alignment in aligned:
            self.assertIn(alignment, expected)

    def test_seqs_to_profiles(self):
        alignment = [[(8, 8), (0, 0), (2, 2), (7, 7), (3, 3), (1, 1), (10, 6), (4, 3), (9, 5)]]
        prof = columns_to_profiles(alignment, 11)
        expected = [[np.array([0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0]),
                     np.array([2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
                     np.array([0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0]),
                     np.array([0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0]),
                     np.array([0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0]),
                     np.array([0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
                     np.array([0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1]),
                     np.array([0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0]),
                     np.array([0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0])]]
        for seq_p, seq_e in zip(prof, expected):
            for pos_p, pos_e in zip(seq_p, seq_e):
                self.assertTrue((pos_p == pos_e).all())
