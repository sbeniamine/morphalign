#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from unittest import TestCase
from morphalign import scoring, align_multi


class TestParadigmAligner(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        letters = ['a', 'ã', 'b', 'e', 'ẽ', 'h', 'i', 'j', 'k', 'kʲ', 'kʷ', 'l', 'lʲ',
                   'n', 'nʲ', 'o', 'õ', 's', 't', 'tʃ', 'tʲ', 'u', 'ũ', 'w', '²', '¹',
                   'ɨ̃', 'ɾ', 'ʃ', 'ʔ', 'ʦ', '⁰']
        cls.int2alpha, cls.alpha2int = scoring.init_alphabet(letters)
        cls.score_matrix = scoring.edit_matrix(len(cls.int2alpha))

    def test_multiple_alignments(self):
        aligner = align_multi.ProgressiveParadigmAligner(self.int2alpha,
                                                         self.alpha2int,
                                                         self.score_matrix)

        forms = [['k', 'e', 'ʔ'],
                 ['k', 'a', 'k', 'e', 'ʔ'],
                 ['n', 't', 'i', 'k', 'e', 'ʔ'],
                 ['k', 'e', 'ʔ'],
                 ['n', 'tʃ', 'a', 'k', 'e', 'ʔ']]

        (alignment, score), *others = aligner.multiple_alignment(forms)

        expected = [['-', '-', '-', '-', '-', '-', 'k', 'e', 'ʔ'],
                    ['-', '-', '-', '-', 'k', 'a', 'k', 'e', 'ʔ'],
                    ['n', 't', 'i', '-', '-', '-', 'k', 'e', 'ʔ'],
                    ['-', '-', '-', '-', '-', '-', 'k', 'e', 'ʔ'],
                    ['n', '-', '-', 'tʃ', '-', 'a', 'k', 'e', 'ʔ']]


        self.assertListEqual(alignment.tolist(), expected)

        # The classical ba/baba
        forms = [['b', 'a'],
                 ['b', 'a', 'b', 'a']]

        ba_infix = [['b', '-', '-', 'a'], ['b', 'a', 'b', 'a']]
        ba_suffix = [['b', 'a', '-', '-'], ['b', 'a', 'b', 'a']]
        ba_prefix = [['-', '-', 'b', 'a'], ['b', 'a', 'b', 'a']]

        # No bias: we should find the three possibilities
        alignments = aligner.multiple_alignment(forms)
        self.assertEqual(len(alignments), 3)
        alignments = [a.tolist() for a, score in alignments]
        self.assertIn(ba_infix, alignments)
        self.assertIn(ba_suffix, alignments)
        self.assertIn(ba_prefix, alignments)

        # Adding a bias should resolve ambiguity
        aligner.bias = "suffix"
        (alignment, score), *others = aligner.multiple_alignment(forms)
        self.assertEqual(len(others), 0)
        self.assertListEqual(ba_suffix, alignment.tolist())

        aligner.bias = "prefix"
        (alignment, score), *others = aligner.multiple_alignment(forms)
        self.assertEqual(len(others), 0)
        self.assertListEqual(ba_prefix, alignment.tolist())

        aligner.bias = "infix"
        (alignment, score), *others = aligner.multiple_alignment(forms)
        self.assertEqual(len(others), 0)
        self.assertListEqual(ba_infix, alignment.tolist())

    def test__restore_indexes(self):
        aligner = align_multi.ProgressiveParadigmAligner(self.int2alpha,
                                                         self.alpha2int,
                                                         self.score_matrix)

        final_state = (((32, 8, 32, 32),
                        (32, 32, 13, 13),
                        (32, 32, 18, 32),
                        (32, 32, 6, 32),
                        (32, 32, 32, 19),
                        (32, 0, 32, 0),
                        (8, 8, 8, 8),
                        (3, 3, 3, 3),
                        (29, 29, 29, 29)),)
        index_map = {0: [0, 3], 1: [1], 2: [2], 3: [4]}
        indexes = [0, 1, 2, 3]
        original_l = 5

        (restored, score), *_ = aligner._restore_indexes(final_state, index_map,
                                                         indexes, original_l)

        expected = [['-', '-', '-', '-', '-', '-', 'k', 'e', 'ʔ'],
                    ['k', '-', '-', '-', '-', 'a', 'k', 'e', 'ʔ'],
                    ['-', 'n', 't', 'i', '-', '-', 'k', 'e', 'ʔ'],
                    ['-', '-', '-', '-', '-', '-', 'k', 'e', 'ʔ'],
                    ['-', 'n', '-', '-', 'tʃ', 'a', 'k', 'e', 'ʔ']]

        self.assertListEqual(restored.tolist(), expected)
