#!/usr/bin/env python
# coding: utf-8

import pandas as pd
from . import representations
import matplotlib.pyplot as plt
import matplotlib.colors as mc
from tqdm import tqdm
import colorsys
from unidecode import unidecode
import logging
log = logging.getLogger()

STYLES = """

table.alignment,
table.infos,
table.infos th,
table.infos td, 
table.infos tr,
table.alignment th,
table.alignment td, 
table.alignment tr {
  border:0;
  border-collapse: collapse;
}

table.infos {
  max-width:90%;
  height:250px;
}

table.alignment.right thead tr:first-of-type th:first-child,
table.infos thead,
table.alignment thead tr:nth-child(2),
table.alignment.right tbody tr th {
  display:none;
}

table.alignment thead tr:nth-child(1) th {
  font-size: 60%;
  text-align:center;
}


table.alignment-infos tbody tr th {
  text-align: left;
  padding-right:20px;
}

table.alignment-infos tbody tr td {
  text-align: left;
  background-color:#fff;
}
table.alignment tbody tr th {
  text-align: left;
  font-variant: small-caps;
}

table.alignment th {
  font-weight:normal;
}

table.alignment tbody td {
  text-align:center;
  vertical-align:middle;
}

table.alignment th, table.alignment td {
  padding:0;
}

table.alignment thead tr:nth-child(2) th:nth-child(even),
tbody td:nth-child(even), tbody tr:hover td {
  background-color: #fafafa;
}

h1 {
clear:both;
}

table.alignment .sound-0x2d {
  color:#ddd;
}
"""


def slug(s):
    return unidecode(s).replace("`", "3").replace("'", "2")


def read_alignments(file):
    tables = {}

    log.info("Reading file %s...", file)

    stems = {}

    def find_exponents_alignment(group):
        l = group["lexeme"].iloc[0]
        forms = group["phon_form"].apply(lambda x: x.split(" ")).tolist()
        codes = group["alignment"].apply(
            lambda x: [int(x) for x in x.split(" ")]).tolist()
        aligned = representations.from_alignment_codes(codes, forms)
        stem, exponents, _ = representations.morphological_segmentation(aligned)
        stems[l] = stem
        group["exponents"] = exponents
        tables[l] = pd.DataFrame(aligned, index=group["cell"])
        tables[l].sort_index(inplace=True)
        return group

    df = pd.read_csv(str(file), index_col=None)
    num_lexemes = df["lexeme"].nunique()
    this_descr = df.language_ID.iloc[0] if "language_ID" in df.columns else file.name
    tqdm.pandas(desc=this_descr, total=num_lexemes, leave=True)
    df = df.groupby("lexeme").progress_apply(find_exponents_alignment).reset_index(drop=True)
    wide_df = df.pivot_table(index="lexeme", columns="cell",
                             values="exponents",
                             aggfunc=lambda overabundant: ";".join(sorted(overabundant)))

    meta_cols = df.columns.intersection(["method", "POS", "language_ID"])
    metadata = df.iloc[0, :][meta_cols].to_dict()
    metadata["vocabulary"] = set(df.phon_form.str.split(r"[\s]").explode().dropna().unique())
    metadata["file"] = file.name
    metadata["stems"] = stems
    return metadata, tables, wide_df


def adjust_lightness(color, amount=0.5):
    ### cf. https://stackoverflow.com/questions/37765197/darken-or-lighten-a-color-in-matplotlib
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return mc.to_hex(colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2]))


def get_microclass(l, df):
    df = df.fillna("#DEF#")
    identical = df[(df == df.loc[l, :]).all(axis=1)]
    return identical.index


def diff_indexes(df1, df2):
    df1 = df1.fillna("#DEF#")
    df2 = df2.fillna("#DEF#")
    diff_idx = (df1 != df2).any(axis=1)
    return df1[diff_idx].index.tolist()


def to_class(sound):
    return "sound-" + "-".join([hex(ord(char)) for char in sound])


def formatter(sound):
    return "<span class='{}'>{}</span>".format(to_class(sound), sound)


def html_alignment(tables, df, lexeme, meta, extra_classes=None):
    alignment = tables[lexeme].applymap(formatter)

    marker_set = get_microclass(lexeme, df)
    meta["markersets"] = len(marker_set)
    extra_classes = extra_classes or []
    infos = pd.Series({
                       "Lexeme": lexeme,
                       "Stem (LCS)": meta["stems"][lexeme],
                       "Marker set size": len(marker_set),
                       "Marker set members examples": ", ".join(marker_set[:5]),
                       "Alignment method": meta["method"],
                       })
    if "language_ID" in meta:
        infos["Language"] = meta["language_ID"]

    lines = ["<div class='example-container'>",
             infos.to_frame().to_html(classes=["infos"] + extra_classes),
             alignment.to_html(escape=False, classes=["alignment"] + extra_classes),
             "</div>"]
    return lines


def html_comparison(html_template, html_structure, args):
    meta_1, t_1, df_1 = read_alignments(args.file)
    meta_2, t_2, df_2 = read_alignments(args.comp)
    if len(df_1.index.intersection(df_2.index)) == 0:
        raise ValueError("The two files have no common lexemes.")

    styles = STYLES + """
        body > div {
          display: flex;
        }
        .example-container {
          flex: 0 0 40%;
                    }
                    
        .example-container:nth-of-type(2) {
          margin-left: 40px;
          flex: 0 0 60%;
        }
        
        """
    if args.lexemes is not None:
        selected = set(args.lexemes)
    else:
        selected = set(diff_indexes(df_1, df_2))
    vocabulary = (meta_1["vocabulary"] | meta_2["vocabulary"]) - {"-"}
    log.info("Writing html files in %s...", args.output)
    colors = cell_colors(vocabulary)
    for l in tqdm(selected):  # id = meta["language_ID"]+"-"+meta["POS"]+"-"+lexeme
        lg_id = meta_1["language_ID"] if "language_ID" in meta_1 else args["file"].name
        id = "{lg_id}-{pos}-{slugl}".format(id=lg_id,
                                           pos=meta_1["POS"],
                                           slugl=slug(l).lower())
        lines = [html_template.format(lang=lg_id,
                                      lexeme=l, pos=meta_1["POS"], id=id)]
        lines.extend(html_alignment(t_1, df_1, l, meta_1, ["left"]))
        lines.extend(html_alignment(t_2, df_2, l, meta_2, ["right"]))
        lines.append("\n</div>")
        this_vocab = (set(t_1[l].melt().value.unique()) |
                      set(t_2[l].melt().value.unique())) - {"-"}
        these_styles = styles + "\n".join(
            ["#" + id + " " + colors[c] for c in this_vocab])
        with (args.output / "comp_{}.ll".format(slug(l))).open("w",
                                                                 encoding="utf-8") as f:
            f.write(html_structure.format(styles=these_styles, content="\n".join(lines)))


def cell_colors(vocabulary):
    lv = len(vocabulary)
    cmap = plt.cm.get_cmap("gist_rainbow", lv)
    colors = [adjust_lightness(cmap(i), 1.6) for i in range(lv)]
    css = {}
    for char, col in zip(vocabulary, colors):
        css[char] = "table.alignment ." \
                    + to_class(char) \
                    + " { display:block;\nbackground-color:" \
                    + col + ";\n}\n"
    return css

