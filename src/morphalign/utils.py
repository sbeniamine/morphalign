from scipy.cluster.hierarchy import to_tree




def _to_ascii(node, leaves, first, next, tot_depth):
    """ Yields lines representing a tree node in ASCII (recursive).

    Args:
        node (scipy.cluster.hierarchy.ClusterNode): a tree node
        leaves (list): list of leaves labels, with indexes corresponding to the ClusterNode ids.
        first (str): a string to use as prefix for the first child
        next (str): a string to use s prefix for the second child
        tot_depth (int): total depth of from the original root to the leaves.

    Yields: strings representing lines of the ASCII tree

    """
    def make_level(d, child, corner, pipe, first, next):
        level = (d - int(child.dist))-1
        branch_prefix =   ('─' * level) + corner if level > 0 else ""
        next_prefix = (' ' * level) + pipe if level > 0 else ""
        return _to_ascii(child,  leaves, branch_prefix + first, next_prefix + next, tot_depth)
    if node.is_leaf():
        pad = " " * (tot_depth - len(first))
        yield pad + leaves[node.id] + " " + first
    else:
        d = int(node.dist)
        yield from make_level(d, node.left,  "┐",  "│", first, next)
        yield from make_level(d, node.right,  "┘",  " ", next, next)

def ascii_tree(dendrogram, leaves, pad=5):
    """  Draw a horizontal ASCII tree from a scipy dendrogram

    Args:
        dendrogram (np.array): hierarchical clustering encoded as a linkage matrix
        leaves (list): list of leaves labels in the hierarchical clustering
        pad (int): the average horizontal length in characters of each level's branches.

    Returns: a string representing the tree in ASCII.
    """
    levels = dendrogram.shape[0]-1
    depth = pad * levels
    mul = depth / dendrogram[-1,2]
    dendrogram[:,2] = (dendrogram[:,2] * mul).round(0)
    tree = to_tree(dendrogram)
    longest_leaf = max(len(x) for x in leaves)
    leaves = [(" "* (longest_leaf-len(label))) + label for label in leaves]
    return "\n".join(_to_ascii(tree, leaves, '', '', depth))
