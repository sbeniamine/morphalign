#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from . import representations
from itertools import combinations
import pandas as pd
from tqdm import tqdm


def avg_percentage_identity(alignment):
    """+ Average percentage identity (PID) (PID4 from Raghva 2006)"""
    def pid_pair(alignment):
        match = alignment[:, 1] == alignment[:, 0]
        no_gaps = alignment != '-'
        identical = (match & no_gaps.all(axis=1)).sum()
        a_len = no_gaps[:,0].sum()
        b_len = no_gaps[:,1].sum()
        shortest_len = min([a_len, b_len])
        return identical / shortest_len

    cols, seqs = alignment.shape
    if seqs == 1:
        return 1
    pids = 0
    pairs = 0
    for i, j in combinations(range(seqs), 2):
        pids += pid_pair(alignment[:,[i,j]])
        pairs += 1
    return (pids / pairs)

def stem_length(stem, shortest_l):
    """Proportion of stem in forms"""
    stem_l = sum([1 for x in stem if x != "-"])
    return (stem_l / shortest_l)

def paradigms_microclass_count(aligned_paradigms):
    wide_df = aligned_paradigms.pivot_table(index="lexeme", columns="cell",
                                            values="exponents",
                                            aggfunc=lambda overabundant: ";".join(sorted(overabundant)))
    return wide_df.drop_duplicates().shape[0]

def paradigms_markers_set_per_cell(aligned_paradigms):
    return aligned_paradigms.groupby("cell").exponents.agg("nunique")

def stem_discontinuity(stem):
    return sum([1 for x in stem.strip("-") if x=="-"])

def eval_per_lexeme(group):
    forms = group["segmental_form"].apply(lambda x: x.split(" ")).tolist()
    codes = group["alignment"].apply(lambda x: [int(x) for x in x.split(" ")]).tolist()
    aligned = representations.from_alignment_codes(codes, forms)
    stem, exponents, _ = representations.morphological_segmentation(aligned)
    group["stem"] = stem
    group["exponents"] = exponents
    group["Avg PID"] = avg_percentage_identity(aligned.T)
    shortest_l = min((len(x) for x in forms))
    group["Stem length ratio"] = stem_length(stem.split(" "), shortest_l)
    return group

def eval_summary(aligned_paradigms, use_tqdm=False):
    num_lexemes = aligned_paradigms["lexeme"].nunique()
    num_cells = aligned_paradigms["cell"].nunique()
    if use_tqdm:
        tqdm.pandas(desc=aligned_paradigms.language_ID[0], total=num_lexemes, leave=False)
        aligned_paradigms = aligned_paradigms.groupby("lexeme").progress_apply(eval_per_lexeme)
    else:
        aligned_paradigms = aligned_paradigms.groupby("lexeme").apply(eval_per_lexeme)

    one_per_lexeme = aligned_paradigms["lexeme"].drop_duplicates().index # get the index of just one form per lexeme

    aligned_paradigms.loc[:, "stem"] = aligned_paradigms.loc[:, "stem"]
    stem_disc = aligned_paradigms.drop_duplicates("lexeme")["stem"].apply(stem_discontinuity)
    exp_count = paradigms_markers_set_per_cell(aligned_paradigms)
    microcl = paradigms_microclass_count(aligned_paradigms)

    results = pd.Series({"Avg. stem discontinuities": stem_disc.mean(),
                         "Avg. Marker set per cell": exp_count.mean(),
                         "Avg PID": aligned_paradigms.loc[one_per_lexeme,"Avg PID"].mean(),
                         "Avg. Stem length ratio": aligned_paradigms.loc[one_per_lexeme,"Stem length ratio"].mean(),
                         "Microclasses": microcl,
                         "total cells": num_cells,
                         "total lexemes": num_lexemes,
                         "Language": aligned_paradigms.language_ID[0],
                         "POS": aligned_paradigms.POS[0],
                         "method": aligned_paradigms["method"][0]})
    return results[["method", "Language", "POS",
                    "Avg. stem discontinuities",
                    "Avg. Marker set per cell", "Avg. Stem length ratio", "Avg PID",
                    "Microclasses", "total cells", "total lexemes"]]


def eval_one(file, threaded=True, **kwargs):
    aligned_paradigms = pd.read_csv(str(file), index_col=None)
    if "alignment" in aligned_paradigms.columns:
        try:
            summary = eval_summary(aligned_paradigms, **kwargs)
            summary["file"] = file.name
            return summary
        except Exception as e:
            if threaded:
                print("{}: {}".format(e.__class__, e))
                print("Ignoring file {}".format(file.name, ))
            else:
                raise e
    return None

