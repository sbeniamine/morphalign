#!/usr/bin/env python3
# -*- coding: utf-8 -*-=
import logging
import numpy as np
from itertools import product, combinations
import distfeat
from scipy.cluster.hierarchy import linkage, fcluster
from scipy.spatial.distance import squareform

log = logging.getLogger()


def sum_of_pairs(alignment, scores):
    """ Sum-of-pairs score for an entire alignment.

    The sum of pair score is the sum of all pairwise scores
     in each column of an alignment.

    Args:
        alignment (list of tuples): the alignment, in column-first format.
        scores (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.

    Returns:

    """
    return sum(scores[a, b] for col in alignment for a, b in combinations(col, 2))


def lcs_bonus_shallow(sound_set, b=-0.1):
    def bonus_if_lcs(col1, col2):
        s = set(col1 + col2)
        if len(s) == 1 and s.pop() in sound_set:
            return b
        return 0
    return bonus_if_lcs

def lcs_bonus_deep(sound_set, b=-0.1):
    def bonus_if_lcs(col1, col2):
        s = col1 + col2
        idx = s.nonzero()[0]
        if idx.shape == 1 and idx[0] in sound_set:
            return b
        return 0

    return bonus_if_lcs
def scorer_shallow(col1, col2, scores):
    """ Score the combination of two columns given as sequences of ints.

    The profile score is the product of all pairwise scores across the two columns.

    It is faster than `scorer_deep` if the columns are small, but does not scale.

    Args:
        col1 (list): characters in a column
        col2 (list): characters in a column
        scores (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.

    Returns: (float) the profile score
    """
    return sum(scores[a, b] for a, b in product(col1, col2))


def scorer_deep(p1, p2, scores):
    """ Score the combination of two columns given as profiles.

    Each profile represents an alignment column.
    A profile is a list of the length of the alphabet, where `p[i]` gives the number of
    times the character denoted by the int `i` is present in the column `p`.

    The profile score is the product of all pairwise scores across the two columns.

    This function is faster than `scorer_shallow` if p1 and p2 are large.

    Args:
        p1 (list): column as a profile: list of counts for all characters.
        p2 (list): column as a profile: list of counts for all characters.
        scores (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.

    Returns: (float) the profile score
    """
    return (scores * p1 * p2[:, None]).sum()


def edit_matrix(l, gap=1, equal=0, sub=20):
    """ Creates a simple edit score matrix for l characters.

    the total `l` includes the gap, which is assumed to be of index `l-1`.

    Args:
        l (int): length of the alphabet + the gap.

    """
    m = np.full((l, l), sub)
    diag = np.eye(l, l, dtype=bool)
    m[diag] = equal
    m[:, -1] = m[-1, :] = gap
    return m


def sim_matrix(phonology, alphabet, alpha2int, method="frisch", clts_path=None):
    """ Creates a score matrix based on shared features from a feature table.

    The similarity score between two phonemes `a` and `b` is the jaccard index (JI)
    between their sets of features. This gives a value between 0 and 1.
    In order for similar sounds to have positive scores, and dissimilar sounds to have
    negative scores, we substract the median between all possible scores.

    The score are then:

    * indels (alignments to gaps): 0
    * identical matches: 1 - median of similarity
    * match between distinct `a` and `b`: JI(fa,fb) - median of similarity


    The similarity score is based on Frisch (1997):
        Stefan Frisch. 1997. Similarity and frequency in phonology. Ph.D. thesis, Northwestern University.

    We use Formal Concept Analysis to compute the sets of natural classes defined
    by the table of distinctive features. The similarity  between two phonemes
     `a` and `b` is the jaccard index (JI) between the sets of natural classes in which
     each participates. This gives a value between 0 and 1.

    Note that this works better with monovalent features (see Frisch 1997).

    In order for similar sounds to have positive scores, and dissimilar sounds to have
    negative scores, we substract the median between all possible scores.

    The score are then:

    * indels (alignments to gaps): 0
    * identical matches: 1 - median of similarity
    * match between distinct `a` and `b`: JI(fa,fb) - median of similarity

    Args:
        phonology (inventory.SoundInventory): interface to sound features
        alphabet (list): mapping of int to str (phonological segments)
        alpha2int (dict): mapping of str (phonological segments) to int

    Returns:
        sim_matrix (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.
    """
    l = len(alphabet)
    gap_idx = l - 1
    sim_matrix = np.zeros((l, l))  # perfect match

    if method == "frisch":
        for i, j in combinations(range(l - 1), 2):
            a, b = alphabet[i], alphabet[j]
            ca = phonology.natural_classes(a)
            cb = phonology.natural_classes(b)
            s = len(ca & cb) / len(ca | cb)
            sim_matrix[i, j] = sim_matrix[j, i] = 1-s

    elif method == "features":
        for i, j in combinations(range(l - 1), 2):
            a, b = alphabet[i], alphabet[j]
            fa = phonology.features_dict(a)
            fb = phonology.features_dict(b)
            names = set(fa).union(fb)
            if len(names) == 0:
                sim_matrix[j, i] = sim_matrix[i, j] = 1
            else:
                sim = sum(fa.get(f, None) == fb.get(f, None) for f in names) \
                      / len(names)
                sim_matrix[j, i] = sim_matrix[i, j] = 1-sim
    elif method == "tresoldi-mielke":
        model = distfeat.DistFeat(clts=clts_path)
        for i, j in combinations(range(l - 1), 2):
            a, b = alphabet[i], alphabet[j]
            sim_matrix[j, i] = sim_matrix[i, j] = model.distance(a, b)
        maxi = sim_matrix.max()
        mini = sim_matrix.min()
        sim_matrix = ((sim_matrix - mini) / (maxi - mini))
    else:
        raise ValueError("I do not know method {}".format(method))

    sim_matrix = adjust_gap_cost(sim_matrix, alphabet, gap_idx)

    return sim_matrix


def adjust_gap_cost(sim_matrix, alphabet, gap_idx):
    """ Find a good threshold for the gap score

    Args:
        sim_matrix: Similarity matrix between sounds
        alphabet (list): list of sounds (labels for the matrix)

    Returns:
        (float) gap score
    """
    m = sim_matrix[:-1, :-1]
    l = m.shape[0]
    triangle = m[np.tril_indices(l - 1, -1)]
    naive_gap_cost = np.quantile(triangle, .5) / 2
    log.debug("Naïve gap cost clusters:")
    log.debug("L < 2G < H //  %s < %s < %s ", np.quantile(triangle[(triangle!=1)], .25),
              np.quantile(triangle[(triangle != 1)], .5),
              np.quantile(triangle[(triangle!=1)], .75))

    # Cluster sounds
    tree = linkage(squareform(m), method="average")
    clusters = fcluster(tree, t=naive_gap_cost*2, criterion='distance')
    n = clusters.max()

    # Take all in-cluster similarities (except the diagonal)
    triangles = []
    for i in range(1, n+1):
        group = (clusters == i)
        lg = group.sum()
        triangles.append(m[group, :][:, group][np.tril_indices(lg, -1)])
        log.debug("cluster %s: %s", i, [a for a,c in zip(alphabet, clusters) if c==i])

    inner_comparisons = np.concatenate(triangles)

    log.debug("With clusters:")
    log.debug("L < 2G < H //  %s < %s < %s ", np.quantile(inner_comparisons, .25),
              np.quantile(inner_comparisons, .5),
              np.quantile(inner_comparisons, .75))

    # The gap score is computed on in-cluster similarities
    gap_cost = np.quantile(inner_comparisons, .5) / 2
    sim_matrix[gap_idx, :] = gap_cost
    sim_matrix[:, gap_idx] = gap_cost

    return sim_matrix


def init_alphabet(alphabet, add_gap=True, gap="-"):
    """ Computes mappings to code sounds on integers.

    Args:
        sounds (list): list of characters.
        add_gap (bool): whether to add a gap at the end of the list of sounds
        gap (str): character representing a gap.

    Returns:
        alphabet (list): mapping of int to str (phonological segments)
        alpha2int (dict): mapping of str (phonological segments) to int
    """
    if add_gap:
        alphabet.append(gap)
    l = len(alphabet)
    alpha2int = dict(zip(alphabet, range(l)))
    return alphabet, alpha2int
