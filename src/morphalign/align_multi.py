#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
import numpy as np
import pandas as pd
from itertools import combinations, product
from collections import defaultdict, Counter
from .align_pairwise import align_pairwise_flat, align_pairwise_shallow, \
    align_pairwise_deep, columns_to_profiles
from . import scoring, representations
from .utils import ascii_tree
from scipy.cluster.hierarchy import linkage  # , dendrogram
from scipy.spatial.distance import squareform
from tqdm import tqdm
from multiprocessing import Pool
from os.path import commonprefix
from os import cpu_count
from tabulate import tabulate

log = logging.getLogger()
# from matplotlib import pyplot as plt



class LexiconAligner(object):
    """Align an entire lexicon table in long form.

    Attributes:
        paradigms (pd.DataFrame): the lexicon in long form.
        aligner (ParadigmAligner): Aligner object. Determines how the multiple alignments are performed.
        cpus (int): the number of cpus used
    """

    def __init__(self, paradigms, aligner, cpu_counts=None, sample=None, **kwargs):
        """
        Args:
            paradigms (pd.DataFrame): the lexicon in long form.
            aligner (ParadigmAligner): Aligner object. Determines how the multiple alignments are performed.
            cpu_counts (int): number of cpus to use
        """
        self.paradigms = paradigms.copy()
        if sample: # For debug purposes, can ask for just a small sample
            lexemes = self.paradigms.lexeme.drop_duplicates()
            l = len(lexemes)
            lexemes_sample = set(lexemes.sample(min(l, sample)))
            self.paradigms = self.paradigms[self.paradigms.lexeme.isin(lexemes_sample)]
        self._candidates = {}
        if cpu_counts is None:
            self.cpus = cpu_count() - 2
        else:
            self.cpus = cpu_counts
        log.info("Using %s cpus", self.cpus)
        self.aligner = aligner
        self.detect_bias()

    def detect_bias(self):
        def count_types(forms):
            if forms.shape[0] < 2:
                return None
            forms = forms.tolist()
            i, prefix = get_prefix(forms)
            j, suffix = get_suffix(forms)
            label = "none"
            if i > 0:
                label = "infix" if j > 0 else "suffix"
            elif j > 0:
                label = "prefix"
            return label

        counts = self.paradigms.groupby("lexeme").agg({"phon_form": count_types})\
                                         .dropna()\
                                          .value_counts(normalize=True)

        # Use entropy to guess whether we're pretty sure there is an inflection type bias...
        # TODO: Is this enough ? What about if different cells have =/= strategies ?
        H = -(counts * np.log2(counts)).sum()
        if H < .8:
            self.aligner.bias = counts.index[0][0]
        logging.debug("Using bias: "+self.aligner.bias)

    def align(self):
        """Align all of the paradigms.

        1. uses the aligner to find all candidate alignments for each paradigm.
        2. desambiguates to pick the best candidate for each paradigm

        Args:
            desamb (str): either of "markers" or "random", determines how the
             disambiguation step is performed.

        Returns:
            paradigms table with extra columns alignments, columns, stem, exponents.
        """

        self.paradigms["phon_form"] = self.paradigms["phon_form"].str.split(" ")
        self.paradigms["alignment"] = ""
        self.paradigms["stem"] = ""
        self.paradigms["exponents"] = ""

        log.debug("######### (1) Searching for candidates #########")
        self._find_candidates()
        log.debug("######### (2) Picking the best candidates #########")
        self._desambiguate()

        verbose = log.isEnabledFor(logging.VERBOSE)
        if verbose:
            self.log_alignments()

        self.paradigms.sort_values(["lexeme", "cell"])
        self.paradigms.reset_index()
        self.paradigms["phon_form"] = self.paradigms["phon_form"].apply(lambda x: " ".join(x))
        return self.paradigms

    def log_alignments(self):
        for lexeme in self._candidates:
            alignment = self._candidates[lexeme]
            tbl = pd.DataFrame(alignment.table, index=alignment.cells, columns=range(1, alignment.table.shape[1]+1))
            log.verbose("# " + lexeme)
            log.verbose("\n\n"+tbl.to_markdown(tablefmt="plain"))

    def _find_candidates(self):
        """ Uses the aligner to find all candidate alignments for each paradigm."""
        pool = Pool(self.cpus)  # Create a multiprocessing Pool
        lexemes = self.paradigms.lexeme.unique().shape[0]
        log.info("Aligning {} lexemes".format(lexemes))
        if self.cpus == 1:
            all_alignments = tqdm(
                map(self._align_paradigm, self.paradigms.groupby("lexeme")),
                total=lexemes, disable=log.isEnabledFor(logging.DEBUG))
        else:
            print(f"using {self.cpus} cpus")
            all_alignments = tqdm(pool.imap_unordered(self._align_paradigm,
                                                      self.paradigms.groupby("lexeme")),
                                  total=lexemes, disable=log.isEnabledFor(logging.DEBUG))
        self._candidates = dict(all_alignments)

    def _align_paradigm(self, item):
        """ Aligns the paradigm of a single lexeme.

        Args:
            item (lexeme, paradigm): the lexeme identifier and its paradigm
        """
        lexeme, paradigm = item
        alignments = self.aligner.multiple_alignment(paradigm["phon_form"])
        cells = paradigm["cell"]
        aligned_objs = []
        for aligned, score in alignments:
            stem, exponents, markers = representations.morphological_segmentation(aligned)
            aligned_objs.append(representations.MorphAlignment(table=aligned, stem=stem,
                                                               markers=markers,
                                                               score=score,
                                                               exponents=exponents,
                                                               cells=cells))
        return (lexeme, aligned_objs)

    def _desambiguate(self):
        """ Resolve ambiguity by picking the most general alignment for each lexeme."""

        # One iteration to calculate stats
        exponent_freq = Counter()
        marker_freq = Counter()
        ambiguities = 0
        for lexeme in self._candidates:
            l = len(self._candidates[lexeme])
            if l > 1:
                ambiguities += 1
            for alignment in self._candidates[lexeme]:
                for cell, exp, markers in zip(alignment.cells, alignment.exponents,
                                              alignment.markers):
                    exponent_freq[(cell, exp)] += 1 / l
                    for exp in markers:
                        marker_freq[(cell, exp)] += 1 / l
        log.debug("%s lexemes with alignment ambiguities.", ambiguities)

        # One iteration to choose the best
        for lexeme, group in self.paradigms.groupby("lexeme"):
            alignments = self._candidates[lexeme]
            debug = log.isEnabledFor(logging.DEBUG) and len(alignments) > 1
            if debug:
                log.debug("desambiguating lexeme: %s", lexeme)
            best_choice = None
            best_score = (float("inf"), float("inf"))
            best_i = 0
            for i, aligned in enumerate(sorted(alignments, key=lambda
                    x: x.exponents)):  # Impose a fixed  order for when scores are identical
                score_exp = sum([exponent_freq[(cell, exp)]
                                 for cell, exp in zip(aligned.cells, aligned.exponents)])
                score_marker = sum([marker_freq[(cell, exp)]
                                    for cell, marker in
                                    zip(aligned.cells, aligned.exponents) for exp in
                                    marker])

                scores = (score_exp, score_marker, aligned.score)
                if debug:
                    exps = ["{}:'{}'={}".format(cell, exp, exponent_freq[(cell, exp)])
                            for cell, exp in zip(aligned.cells, aligned.exponents)]
                    log.debug(
                        "Alignment (%s)\tstem %s\tscore: exponents %s; markers %s; sum of pairs %s\t%s",
                        i, aligned.stem, *scores, "\t".join(exps))
                if scores <= best_score:
                    best_choice = aligned
                    best_score = scores
                    best_i = i

            self._candidates[lexeme] = best_choice
            self.paradigms.loc[group.index, "stem"] = best_choice.stem
            self.paradigms.loc[group.index, "exponents"] = best_choice.exponents
            self.paradigms.loc[
                group.index, "alignment"] = representations.to_alignment_codes(
                best_choice.table)
            if debug:
                log.debug("chose (%s)", best_i)


class ParadigmAligner(object):
    """Align the paradigm of a single lexeme."""

    def __init__(self, int2alpha, alpha2int, score_matrix,
                 iterative_refinement=True,
                 score_blur=0, max_ambiguity=30, **kwargs):
        """
        Args:
            int2alpha (list): mapping of int to str (phonological segments)
            alpha2int (dict): mapping of str (phonological segments) to int
            score_matrix (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.
            score_blur (int):
            max_ambiguity (int):
            **kwargs:
        """
        self.scores = score_matrix
        self.alpha2int = alpha2int
        self.int2alpha = int2alpha
        self.score_blur = score_blur
        self.max_ambiguity = max_ambiguity
        self.iterative_refinement = iterative_refinement
        self.bias = "none"  # default to no biases

    def alignment_string(self, aligned, col_first=True):
        """ Creates a string to represent the alignment(s).

        Args:
            aligned (np.array): the alignment(s). It can contain either ints or characters.
                If there are three dimensions, this is a sequence of alignments, if there
                are two, this is a single alignment. Alignments are given either
                columns-first or row-first.
            col_first (bool): whether the alignments are given column- first.

        Returns: (str) a string representing the alignment
        """
        aligned = np.array(aligned, dtype=object)

        def restore(i):
            try:
                return self.int2alpha[i]
            except TypeError:
                raise
                return str(i)

        def align_one(matrix):
            if col_first:
                matrix = matrix.T
            return [tabulate([[restore(i) for i in seq] for seq in matrix], tablefmt='plain')]

        if len(aligned.shape) == 3:
            strs = []
            for al in aligned:
                strs += align_one(al)
                strs += [""]
        else:
            strs = align_one(aligned)
        return "\n".join(strs)

    def multiple_alignment(self, strings):
        """ Perform iterative multiple alignments over a set of strings.

        Args:
            strings (list): list of strings

        Returns:

        """
        raise NotImplementedError("This is an abstract class, please use a child class.")

    def _guide_tree(self, sequences):
        """ Creates a guide tree based on pairwise distances.

        This uses plain levenshtein distances (+possible bias),
         bc we do want to align sequences that share more material first.

        Input sequences are lists of tuples, each containing a single int-coded sound,
        for ex: "abc" could be [(1,), (2,), (3,)]

        Args:
            sequences: List of list of integers, each inner list represents a string.

        Returns:
            guide tree (np.array): A linkage matrix.
        """
        l = len(sequences)
        scores = scoring.edit_matrix(self.scores.shape[0])
        dist = np.zeros((l, l))  # be careful, initializing to 0
        for i, j in combinations(range(l), 2):
            score, _ = align_pairwise_flat(sequences[i], sequences[j],
                                           scores, ambiguity=False,
                                           bias=self.bias)
            dist[i, j] = dist[j, i] = score

        guide_tree = linkage(squareform(dist), method=self.linkage_method)

        if log.isEnabledFor(logging.DEBUG):
            sequences_str = [
                " ".join([self.int2alpha[x[0]] for x in seq]) + " ({})".format(i) for
                i, seq in
                enumerate(sequences)]
            log.debug("Guide tree:\n%s", ascii_tree(guide_tree, sequences_str, pad=6))

        return guide_tree

    def _code_sequences(self, strings):
        """ Code sequences of strings on integers, without duplicates.

        - Code segments on integers
        - Remove duplicate sequences
        - keep a map of sequences to original indexes,
        in order to restore duplicates later on


        Args:
            strings: List of list of characters. Each inner list represents a string.

        Returns:
            index_map: a defaultdict of index in the result to a list of original indexes
            sequences: a list of tuples, each inner tuple is a list of sequence,
              each sequence is a tuple of ints
        """
        sequences = []  # This is
        seq_to_idx = defaultdict(list)  # sequences to index in sequences
        index_map = defaultdict(
            list)  # index in original strings to list of indexes in sequences
        for j, segmented in enumerate(strings):
            seq = tuple((self.alpha2int[char],) for char in
                        segmented)  # code on integers, single character per column.
            if seq in seq_to_idx:
                i = seq_to_idx[seq]
            else:
                i = len(sequences)
                sequences.append(seq)
                seq_to_idx[seq] = i
            index_map[i].append(j)
        return index_map, sequences

    def refine_iteratively(self, alignment, idxes=None, lcs_sounds=None):
        """ Refines the alignment iteratively

        Args:
            alignment (list): list of tuples. Inner lists are columns in the alignments.
                values are ints coding characters.
            idxes (iterable of int): the indexes of the sequences for which to perform
                iterative refinement. If this is None (default), refine all sequences.

        Returns:
            score (float): the new score of the alignment
            alignment (tuple of tuples): the new alignment
        """
        log.debug("=========== ITERATIVE REFINEMENT ===========")
        alphabet_l = len(self.scores)
        gap = alphabet_l - 1

        def as_tuple(array):
            return tuple(tuple(x) for x in array)

        def remove_gap_cols(alignment):
            return [col for col in alignment if not all([c == gap for c in col])]

        l = len(alignment[0])
        if idxes is None:
            idxes = [[i] for i in range(l)]

        prev_score = float("inf")
        best_score = scoring.sum_of_pairs(alignment, self.scores)
        log.debug("initial alignment\n%s", self.alignment_string(alignment))
        new_alignment = alignment

        # new_alignment = alignment
        # changed = False
        while best_score < prev_score:

            prev_score = best_score
            new_alignment = alignment

            for indexes in idxes:
                base = remove_gap_cols(np.delete(new_alignment, indexes, axis=1))
                inverse = [i for i in range(l) if i not in indexes]
                other = remove_gap_cols(np.delete(new_alignment, inverse, axis=1))
                bp = columns_to_profiles([base], alphabet_l)[0]
                op = columns_to_profiles([other], alphabet_l)[0]
                _, aligned = align_pairwise_deep(as_tuple(base), as_tuple(other),
                                                 bp, op,
                                                 self.scores, ambiguity=False,
                                                 bias=self.bias, lcs_sounds=lcs_sounds)
                score = scoring.sum_of_pairs(aligned, self.scores)
                aligned = np.array(aligned)
                left_l = l-len(indexes)
                index_map = list(range(left_l))
                for i in range(len(indexes)):
                    index_map.insert(indexes[i], left_l+i)  # we moved the base indexes to 0
                candidate_alignment = aligned[:, index_map]  # restore seq order

                if score < best_score:
                    best_score = score
                    new_alignment = candidate_alignment

            if best_score < prev_score:
                log.debug("Found score %s better than %s", score, prev_score)
                log.debug("with alignment\n%s", self.alignment_string(new_alignment))
                alignment = new_alignment

            log.debug("score=%s (prev %s) at step %s", score, prev_score, indexes)
            log.debug("Before:\n%s", self.alignment_string(alignment))
            log.debug("After:\n%s", self.alignment_string(new_alignment))

        return score, as_tuple(alignment)


class ProgressiveParadigmAligner(ParadigmAligner):
    """Align the paradigm of a single lexeme using progressive alignments.

    See:
        - Da-Fei Feng and Russell F. Doolittle. 1987. Progressive sequence alignment as a
        prerequisite to correct phylogenetic trees. Journal of Molecular Evolution,
        25(4):351–360
        - Richard Durbin. 1998. Biological Sequence Analysis: Probabilistic
        Models of Proteins and Nucleic Acids. Cambridge University Press.
    """

    def __init__(self, *args, linkage_method="average", **kwargs):
        """

        Args:
            *args: see parent class ParadigmAligner
            linkage_method: the linkage method used for the guide tree.
            **kwargs:
        """
        super().__init__(*args, **kwargs)
        self.linkage_method = linkage_method

    def multiple_alignment(self, strings):
        """ Perform progressive multiple alignments over a set of strings.

        Args:
            strings: a sequence of forms. Each form is a list of strings.
                Each string is a phoneme. The sequence is usually a pd.Series

        Returns:
            result: a list of list of characters. Each inner list is a column.

            ex:

                a  b -  c
                a  - f  c

            is coded as:

            >>> [["a", "a"], ["b", "-"], ["-", "f"], ["c", "c"]]
        """
        original_l = len(strings)
        index_map, sequences = self._code_sequences(strings)

        if len(sequences) == 1:  # only a single unique form, we can align them all left
            columns = zip(*strings)  # align left
            alignment = [list(x) for x in zip(*columns)]  # align
            return [(alignment, 1) ]

        guide_tree = self._guide_tree(sequences)
        return self._progressive(sequences, guide_tree, index_map, original_l)

    def _progressive(self, sequences, guide_tree, index_map, original_l, lcs_sounds=None):
        l = len(sequences)
        alphabet_l = len(self.scores)
        # Align along tree
        clusters = [(seq,) for seq in sequences]
        # each cluster node may contain several alternate alignments.
        indexes = [[i] for i in range(l)]
        for step, (i, j, dist, obs) in enumerate(guide_tree):
            i = int(i)
            j = int(j)
            step = l + step

            seqs_left = clusters[i]
            seqs_right = clusters[j]

            profiles_left = columns_to_profiles(seqs_left, alphabet_l)
            profiles_right = columns_to_profiles(seqs_right, alphabet_l)

            alignment_set = set()
            min_score = None
            for m, n in product(range(len(seqs_left)),
                                range(len(
                                    seqs_right))):  # keeping ambiguity in the tree leaves
                s1 = seqs_left[m]
                s2 = seqs_right[n]

                if len(s1) * len(s2) < alphabet_l:
                    score, alignments = align_pairwise_shallow(s1, s2, self.scores,
                                                               bias=self.bias,
                                                               lcs_sounds=lcs_sounds)
                else:
                    score, alignments = align_pairwise_deep(s1, s2,
                                                            profiles_left[m],
                                                            profiles_right[n],
                                                            self.scores,
                                                            bias=self.bias,
                                                            lcs_sounds=lcs_sounds)
                alignments = alignments[:self.max_ambiguity]
                # Update the set of best alignments
                new_profiles = [tuple(aligned) for aligned in alignments]
                if min_score is None:
                    min_score = score
                    alignment_set = set(new_profiles)
                else:
                    blur_amount = self.score_blur * min_score
                    if (score + blur_amount) < min_score:
                        min_score = score
                        alignment_set = set(new_profiles)
                    elif (score - blur_amount) <= min_score:
                        alignment_set = alignment_set.union(new_profiles)
            # this limits the number of retained concurrent alignments # Maybe remove ?

            clusters.append(tuple(alignment_set)[:self.max_ambiguity])


            log.debug("(%s)=(%s)+(%s) score=%s ambiguity=%s:", step, i, j, round(score,4), len(alignment_set))
            for al in alignment_set:
                log.debug("\n"+self.alignment_string(list(al)))
            indexes.append(indexes[i] + indexes[j])
        final_state = clusters[-1]

        # Iterative refinement
        if self.iterative_refinement:
            final_state = set()
            offsets = dict(zip(indexes[-1],range(l)))
            nodes = [[offsets[i] for i in node] for node in indexes[:-1]][::-1]
            for hypothesis in alignments:
                score, aligned = self.refine_iteratively(hypothesis,
                                                         idxes=nodes,
                                                            lcs_sounds=lcs_sounds)
                final_state.add(aligned)
            final_state = tuple(final_state)

        if log.isEnabledFor(logging.DEBUG) and len(final_state) > 1:
            log.debug("Multiple candidates were found:\n%s",
                      self.alignment_string(final_state))


        # Map each sequence back to its original position
        return self._restore_indexes(final_state, index_map, indexes[-1], original_l)

    def _restore_indexes(self, final_state, index_map, final_indexes, original_l):
        """

        Args:
            final_state:
            index_map:
            final_indexes:
            original_l:

        Returns:

        """

        tree_index_map = [None for _ in range(original_l)]
        for i, j in enumerate(final_indexes):
            for m in index_map[j]:
                tree_index_map[m] = i

        result = []
        for alignment in final_state:
            alignment = np.array(alignment) # array of columns
            alignment = alignment[:,tree_index_map]  # Restore original duplicates for sequences
            score = scoring.sum_of_pairs(alignment, self.scores)
            alignment = np.array([[self.int2alpha[i] for i in col] for col in alignment.T])
            result.append((alignment, score))
        return result


class LCSProgressiveParadigmAligner(ProgressiveParadigmAligner):

    def find_LCS(self, sequences):
        def substrings(form, n=2):
            return set(combinations(form, r=n))

        # Simplify task by keeping only chars common to all sequences
        chars = set.intersection(*[{c[0] for c in s} for s in sequences])
        sequences = [[c[0] for c in seq if c[0] in chars] for seq in sequences]

        # Simplify task by removing initial common prefix and suffix
        i, prefix = get_prefix(sequences)
        j, suffix = get_suffix(sequences, start=i)
        sequences = [s[i:len(s) - j] for s in sequences]

        # Now, look for central LCS with brute force
        # This is quick because the previous steps made the strings overall very short
        # This was Matias's idea :)
        k = min([len(s) for s in sequences])
        for length in range(k, 0, -1):
            centers = set.intersection(
                *[substrings(s, length) for s in sequences])
            if centers:
                return {prefix + c + suffix for c in centers}
        return {prefix + suffix}

    def _progressive(self, sequences, *args):
        """ Separate progressive alignments for each possible LCS.

        - find a set of possible LCS
        - For each LCS:
            - To ensure a specific LCS is favored, add a bonus to identical columns of this sound.
            - run progressive alignments
        - aggregate results over all LCS
        """
        lcs_set = self.find_LCS(sequences)
        result = []
        for lcs in lcs_set:
            log.debug("with LCS: %s", [self.int2alpha[i] for i in lcs])

            # Align, with a LCS bonus for identical columns of sounds in the LCS
            alignments = super()._progressive(sequences, lcs_sounds=lcs, *args)
            result.extend(alignments)
        return alignments

def get_prefix(sequences):
    prefix = tuple(commonprefix(sequences))
    i = len(prefix)
    return i, prefix

def get_suffix(sequences, start=0):
    suffix = tuple(commonprefix([s[start:][::-1] for s in sequences])[::-1])
    j = len(suffix)
    return j, suffix
