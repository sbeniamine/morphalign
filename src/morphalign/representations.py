#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from collections import namedtuple

MorphAlignment = namedtuple('MorphAlignment',
                            ['table', 'stem', 'exponents','markers', 'cells', 'score'])

def morphological_segmentation(alignment):
    """ Extract stem, entire exponents, and markers from an alignment.

    Defiitions of the extracted elements:

    * stem: the discontinuous string of material which is identical in all rows.
    * exponents: a list of the discontinuous string of the forms minus the stem.
    * markers: exponents are cut on their discontinuities, so that each form
     is represented by a list of continuous substrings.

    In the stem and exponents,  discontinuities are marked by the gap "-".
    sounds and gap are separated by spaces.

    Example:
        >>> stem, exp, markers = morphological_segmentation([("m","ɛ","l","-","-","-"),
        ...                                                  ("m","e","l","-","-","e"),
        ...                                                  ("m","ɛ","l","ø","r","e")])
        >>> stem
        "m - l -"
        >>> exp
        ['- ɛ -', '- e - e', '- ɛ - ø r e']
        >>> markers
        [['ɛ'], ['e', 'e'], ['ɛ', 'ø r e']]

    Args:
        alignment (list): alignment matrix (list of list, or list of tuples)
            the alignment matrix is given row-firsts.

    Returns: stem (str), exponents (list of strings) and markers (list of lists).
    """
    alignment = np.array(alignment)
    l, m = alignment.shape
    stem = []
    non_stem = [[] for _ in range(l)]

    for i in range(m):
        char = alignment[0, i]
        if char != "-" and (alignment[:, i] == alignment[0, i]).all():
            stem.append(char)
            for elem in non_stem:
                if elem == [] or elem[-1] != "-":
                    elem.append("-")
        else:
            for j in range(l):
                char = alignment[j, i]
                if char != "-":
                    non_stem[j].append(char)
            if stem == [] or stem[-1] != "-":
                stem.append("-")

    exponents = []
    markers = []
    for elem in non_stem:
        exp = " ".join(elem)
        exponents.append(exp)
        markers.append([x.strip(" ") for x in exp.split("-") if x and not x.isspace()])
    return " ".join(stem), exponents, markers


def from_alignment_codes(codes, forms, fillvalue="-"):
    """ Returns aligned forms from alignment codes and a list of forms.

    The forms give a paradigm as a list of words, where each word is a list of phonemes.
    The codes are a list of list of ints of the same exact dimensions.
    The ints are co-indexed with the phonemes in forms, and indicate the alignment
    column to which to assign the phoneme.

    /!\\ codes is not the raw output of `to_alignment_codes`, which returns a list of space
    separated strings, rather than a list of list. This is practical when exporting.


    Example:
        >>> forms = [["m","ɛ","l"], ["m", "e", "l", "e"], ["m", "ɛ", "l", "ø", "r", "e"]]
        >>> codes = [[ 0,  1,  2],  [ 0,   1,   2,   5],  [ 0,   1,   2,   3,   4,   5]]
        >>> from_alignment_codes(codes, forms)
        np.array([["m","ɛ","l","-","-","-"],
                  ["m","e","l","-","-","e"],
                  ["m","ɛ","l","ø","r","e"]])

    Args:
        codes (list): list of strings representing integer positions in alignments.
        forms (list): list of iterables representing segments in forms.

    Returns: (np.array): alignment of the segments in the forms according to the codes.

    """
    l = len(forms)
    assert l == len(codes), "Alignment codes should have as many indexes as form segments"
    total_cols = max([max(cols) for cols in codes]) + 1
    alignments = np.full((l, total_cols), fillvalue, dtype=object)
    for i in range(l):
        assert len(forms[i]) == len(codes[i]), "{} is not a valid alignment sequence" \
                                       " for {} (differing lengths)".format(codes[i], forms[i])
        alignments[i, codes[i]] = forms[i]

    # Remove any extra column which has only gaps
    only_gap = np.array([fillvalue for _ in range(alignments.shape[1])])
    alignments = alignments[:, ~(alignments == only_gap).all(axis=0)]
    return alignments

def to_alignment_codes(alignment):
    """ Returns alignment codes for a given alignment.

    The codes are a list of space separated integers. Each integer is coindexed
     with a phoneme in aligned forms, and indicate the alignment column
     to which to assign the phoneme.

    Example:
        >>> alignment = np.array([["m","ɛ","l","-","-","-"],
        ...                       ["m","e","l","-","-","e"],
        ...                       ["m","ɛ","l","ø","r","e"]])
        >>> to_alignment_codes(alignment)
        ["0 1 2", "0 1 2 5", "0 1 2 3 4 5"]
        >>> from_alignment_codes(codes, forms)


    Args:
        alignment (np.array): alignment matrix (list of list, or list of tuples)
            the alignment matrix is given row-firsts.

    Returns:
        codes (list): list of strings, each string is the alignment code for a form.

    """
    alignment = np.array(alignment)
    l, m = alignment.shape
    result = []
    for i in range(l):
        result.append(" ".join([str(j) for j in range(m) if alignment[i, j] != "-"]))
    return result