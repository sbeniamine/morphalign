#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from .scoring import scorer_shallow, scorer_deep, lcs_bonus_shallow, lcs_bonus_deep
from itertools import product


def columns_to_profiles(sequence, alphabet_l):
    """ Converts an alignment given as a sequence of columns to a sequence of profiles.

    A profiles represents the counts of sounds in an alignment column.
    A profile is a list `p` of the length of the alphabet,
    where `p[i]` gives the number of times the character denoted by
    the int `i` is present in the column `p`.

    Args:
        sequence (list): list of lists (or tuples) where the outer list represents
            an alignment or a form and the inner lists represent columns. Columns
            contain `int`s denoting sounds.
        alphabet_l (int): length of the alphabet

    Returns: profiles (list): list of lists where the inner list are given as profiles.

    """

    def alpha_counts(position):
        """This creates a profile position, but normalizes gap counts: we only ever count a single gap."""
        prof = np.bincount(position, minlength=alphabet_l)
        #prof[-1] = min(prof[-1], 1)
        return prof

    return [[alpha_counts(x) for x in s] for s in sequence]


def align_pairwise_flat(s1, s2, scores, ambiguity=True, **kwargs):
    """Pairwise alignment function, assumes each sequence is a single form.

    Args:
        s1, s2 (list or tuple):  two sequences of tuples representing the forms
         to be aligned. Tuples are columns, in this specific case, they contain a single int.
            ex: ((8,), (0,), (2,), (7,))
        scores (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.
        ambiguity (bool): if True (default), returns all non-equivalent alignments of
            minimal score. If False, pick a single alignment.
        **kwargs: extra arguments passed to _align_pairwise

    Returns:
        score (float): score of the entire alignment
        alignment (list): list of alignment, where each alignment is a list of tuples.
         inner tuples are columns in the alignment.
    """
    l = len(scores)
    gap = l - 1
    paths = _align_pairwise(s1, s2, scores, [gap] * len(s1[0]), [gap] * len(s2[0]),
                            lambda a, b, scores: scores[a[0], b[0]], **kwargs)

    trackback = _backtrack_iter
    if ambiguity:
        trackback = _multibacktrack_iter
    return paths[-1][-1][0][0], trackback(paths, s1, s2, l - 1)


def align_pairwise_shallow(s1, s2, scores, ambiguity=True, lcs_sounds=None, **kwargs):
    """ Pairwise alignment function between alignments. Optimized for shallow alignments.

    Args:
        s1, s2 (list or tuple): two sequences of tuples representing the alignments
         to be aligned. Each tuple is a column with aligned segments coded on ints.
         ex: [(6, 6), (3, 3), (23, 23), (30, 30)]
        scores (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.
        ambiguity (bool): if True (default), returns all non-equivalent alignments of
            minimal score. If False, pick a single alignment.
        **kwargs: extra arguments passed to _align_pairwise

    Returns:
        score (float): score of the entire alignment
        alignment (list): list of alignment, where each alignment is a list of tuples.
         inner tuples are columns in the alignment.
    """
    """Pairwise alignment function. Faster with shallow profiles"""
    l = len(scores)
    gap = l - 1
    bonus_func = lcs_sounds if lcs_sounds is None else lcs_bonus_shallow(lcs_sounds)
    paths = _align_pairwise(s1, s2, scores, [gap] * len(s1[0]), [gap] * len(s2[0]), scorer_shallow,
                            lcs_bonus=bonus_func, **kwargs)

    trackback = _backtrack_iter
    if ambiguity:
        trackback = _multibacktrack_iter
    return paths[-1][-1][0][0], trackback(paths, s1, s2, l - 1)


def align_pairwise_deep(s1, s2, s1_profiles, s2_profiles, scores, ambiguity=True, lcs_sounds=None, **kwargs):
    """ Pairwise alignment function between alignments. Optimized for deep alignments.

    In order to be efficient on alignments with many sequences, this functions relies on
    profiles. A profile is a list of the length of the alphabet,
    where `p[i]` gives the number of times the character denoted by the int `i` is
    present in the column `p`.

    We take these in argument rather than convert directly from the columns, because
    this makes it possible to store profiles rather than recompute them.

    Args:
        s1, s2 (list or tuple): List of columns in the two the alignments
         to be aligned. Each tuple is a column with aligned segments coded on ints.
         ex: [(6, 6), (3, 3), (23, 23), (30, 30)]
        s1_profiles (list): list of profiles encoding sequence s1
        s2_profiles (list): list of profiles encoding sequence s2
        scores (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.
        ambiguity (bool): if True (default), returns all non-equivalent alignments of
            minimal score. If False, pick a single alignment.
        **kwargs: extra arguments passed to _align_pairwise

    Returns:
        score (float): score of the entire alignment
        alignment (list): list of alignment, where each alignment is a list of tuples.
         inner tuples are columns in the alignment.
    """
    l = len(scores)
    left_gap = np.zeros(l)
    left_gap[-1] = len(s1[0])
    right_gap = np.zeros(l)
    right_gap[-1] = len(s2[0])
    bonus_func = lcs_sounds if lcs_sounds is None else lcs_bonus_deep(lcs_sounds)
    paths = _align_pairwise(s1_profiles, s2_profiles, scores,
                            left_gap, right_gap, scorer_deep,
                            lcs_bonus=bonus_func, **kwargs)

    trackback = _backtrack_iter
    if ambiguity:
        trackback = _multibacktrack_iter
    return paths[-1][-1][0][0], trackback(paths, s1, s2, l - 1)

## Todo: correct biases
def prefix_bias(n, max=1):
    """ Prepare a bias array for prefixal languages.
    Return an array of length n, with values scales geometrically between 0 and max.
    """
    if n == 1: return np.array([0])
    return (np.geomspace(0.1, max+0.1, num=n)-0.1).round(4)


def suffix_bias(n, max=1):
    """ Prepare a bias array for suffixal languages.
    Return an array of length n, with values scales geometrically between max and 0.
    """
    if n == 1: return np.array([0])
    return (np.geomspace(max+0.1, 0.1, num=n) - 0.1).round(4)


def infix_bias(n, max=1):
    """ Prepare a bias array for infixal languages.
    Return an array of length n, with values scaled geometrically between max and 0.
    """
    if n == 1: return np.array([0])
    half = (n // 2)
    if (n % 2) > 0:
        left = suffix_bias(half + 1, max=max)
        right = prefix_bias(half + 1, max=max)[1:]  # trim the 0 on the left
    else:  # two zeros in the middle
        left = suffix_bias(half, max=max)
        right = prefix_bias(half, max=max)

    return np.concatenate((left, right))


BIASES = {"prefix": prefix_bias,
          "infix": infix_bias,
          "suffix": suffix_bias,
          "none": np.zeros
          }


def _align_pairwise(s1, s2, scores, left_gap, right_gap, scorer,
                    bias="none", lcs_bonus=None, **kwargs):
    """ Fill the dynamic matrix with scores and paths.

    Args:
        s1, s2 (list or tuple): List of columns in the two the alignments
         to be aligned. Each tuple is a column with aligned segments coded on ints.
         ex: [(6, 6), (3, 3), (23, 23), (30, 30)]
        scores (np.array): scoring matrix. scores[i, j] is the cost of aligning i and j.
        left_gap (list or np.array): sequence of gaps of length equal to `s1`'s column depth
        right_gap (list or np.array): sequence of gaps of length equal to `s2`'s column depth
        scorer: scoring function which takes two columns or profiles and returns a score.
            (this can be either of scoring.scorer_deep or scoring.scorer_shallow)
    Returns:
        paths (np.array): matrix with scores and paths.
    """
    m = len(s1)
    n = len(s2)
    paths = np.empty((m + 1, n + 1), dtype=list)
    paths[0, 0] = [(0, (0, 0))]

    # Get the func
    bias_f = BIASES[bias]
    if lcs_bonus is None:
        lcs_bonus = lambda *args:0

    # Compute gap scores, add the prefix/infix/suffix bias
    # print(bias_f(m), bias_f(n))
    bias_a = bias_f(m)
    bias_b = bias_f(n)
    gap_cost_a = np.array([scorer(a, right_gap, scores) for a in s1]) + bias_a
    gap_cost_b = np.array([scorer(left_gap, b, scores) for b in s2]) + bias_b

    for i in range(m):
        paths[i + 1, 0] = [(paths[i, 0][0][0] + gap_cost_a[i], (i, 0))]

    for j in range(n):
        paths[0, j + 1] = [(paths[0, j][0][0] + gap_cost_b[j], (0, j))]

    for (i, a), (j, b) in product(enumerate(s1), enumerate(s2)):
        subcost = paths[i, j][0][0] + scorer(a, b, scores) + lcs_bonus(a, b)
        insa = paths[i, j + 1][0][0] + gap_cost_a[i]
        insb = paths[i + 1, j][0][0] + gap_cost_b[j]

        arrows = [(insb, (i + 1, j)),
                  (insa, (i, j + 1)),
                  (subcost, (i, j))]
        mini = min((subcost, insa, insb))
        paths[i + 1, j + 1] = [arrow for arrow in arrows if arrow[0] == mini]
    return paths


def _backtrack_iter(paths, s1, s2, gap):
    """ Backtrack through the score & paths matrix to extract a single best alignment.

    Args:
        paths (np.array): matrix with scores and paths
        s1, s2 (list or tuple): List of columns in the two the alignments
         to be aligned. Each tuple is a column with aligned segments coded on ints.
         ex: [(6, 6), (3, 3), (23, 23), (30, 30)]
        gap (int): index representing a gap.

    Returns:
        a single alignment as a list of tuples. Each tuple is a column.
    """
    min_i, min_j = paths.shape
    left_gap = tuple(gap for _ in range(len(s1[0])))
    right_gap = tuple(gap for _ in range(len(s2[0])))
    path = []
    i, j = min_i - 1, min_j - 1
    while i != 0 or j != 0:
        _, idxs = paths[i, j][0]
        if idxs == (i - 1, j):  # insert
            action = [s1[i - 1] + right_gap]
        elif idxs == (i, j - 1):  # del
            action = [left_gap + s2[j - 1]]
        else:  # substitution
            action = [s1[i - 1] + s2[j - 1]]
        path = action + path
        i, j = idxs
    return path


def _multibacktrack_iter(paths, s1, s2, gap):
    """ Backtrack through the score & paths matrix to extract all best alignments.

    Gives all non-interchangeable alignments with minimum score.
    Two alignments are considered non-interchangeable if they differ by at least
    one substitution. These are interchangeable:
        h-ello   he-llo
        ha-llo   h-allo

    But they are distinct from::
        hello
        hallo

    Args:
        paths (np.array): matrix with scores and paths
        s1, s2 (list or tuple): List of columns in the two the alignments
         to be aligned. Each tuple is a column with aligned segments coded on ints.
         ex: [(6, 6), (3, 3), (23, 23), (30, 30)]
        gap (int): index representing a gap.

    Returns:
        a single alignment as a list of tuples. Each tuple is a column.
    """
    min_i, min_j = paths.shape
    stack = [([],  # empty alignment
              (min_i - 1, min_j - 1),  # start at last cell
              set())  # No cell is visited yet.
             ]
    left_gap = tuple(gap for _ in range(len(s1[0])))
    right_gap = tuple(gap for _ in range(len(s2[0])))
    solutions = []
    while stack != []:
        current_path, (i, j), visited = stack.pop(0)
        if (i, j) in visited:
            continue  # abandon this path, it is redundant
        else:
            visited.add((i, j))
            if i == 0 and j == 0:
                solutions.append(current_path)
            else:
                for step in paths[i, j]:
                    _, idxs = step
                    new_visited = visited
                    if idxs == (i - 1, j):  # insert
                        action = [s1[i - 1] + right_gap]
                    elif idxs == (i, j - 1):  # insert
                        action = [left_gap + s2[j - 1]]
                    else:  # substitution
                        new_visited = set()
                        action = [s1[i - 1] + s2[j - 1]]

                    stack.append((action + current_path,
                                  idxs,
                                  new_visited))
    return solutions
