#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from . import eval, scoring, align_multi, html_export
import pandas as pd
import logging
import argparse
from pathlib import Path
from multiprocessing import Pool
from tqdm import tqdm
from inventory.sounds import SoundInventory

logging.VERBOSE = 15
logging.addLevelName(logging.VERBOSE, "VERBOSE")
logging.Logger.verbose = lambda inst, msg, *args, **kwargs: inst.log(logging.VERBOSE, msg, *args, **kwargs)


def eval_command():
    """Evaluate alignments"""
    import argparse
    usage = eval_command.__doc__
    parser = argparse.ArgumentParser(description=usage,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("paradigms",
                        help="paradigm directory or file, full path -- long shape, csv",
                        type=str)
    parser.add_argument("-o", "--output", help="Output file path", type=str, default=None)
    parser.add_argument("--cpu",
                        help="cpu cores", type=int, default=None)
    args = parser.parse_args()

    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    log = logging.getLogger()
    log.info(args)

    rows = []
    input = Path(args.paradigms)
    if input.is_dir():
        pool = Pool(args.cpu)  # Create a multiprocessing Pool
        files = list(input.glob("*.csv"))
        rows = tqdm(pool.imap_unordered(eval.eval_one, files), total=len(files))
        rows = [r for r in rows if r is not None]
    else:
        summary = eval.eval_one(input, use_tqdm=True, threaded=False)
        if summary is not None:
            rows.append(summary)

    if rows:
        eval_result = pd.DataFrame(rows).set_index("method")

        if args.output is None:

            with pd.option_context('display.max_colwidth', 1000):
                for name, group in eval_result.groupby(["Language", "POS"]):
                    group = group.sort_values(by=["Microclasses"], ascending=False)
                    print(*name)
                    print(group.round(2))
                    print("\n")
        else:
            eval_result.to_csv(args.output)


def align_command():
    """Run alignments on a given paradigm table"""

    usage = align_command.__doc__

    parser = argparse.ArgumentParser(description=usage, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("paradigms", help="paradigm file, full path -- long shape, csv", type=str)
    parser.add_argument("output", help="Output file path", type=str)

    phonology_args = parser.add_mutually_exclusive_group()
    phonology_args.add_argument("-f", "--features",
                                help="features file, full path (csv) or 'bipa' (default)", type=str, default="bipa")
    parser.add_argument('-a', '--algorithm', help="Algorithm used",
                        choices=['Progressive', 'LCSProgressive'], default='Progressive')
    parser.add_argument('-s', '--scores', help="Score matrix type",
                        choices=['simple', 'frisch', 'features'], default='simple')
    parser.add_argument("--cpu", help="cpu cores", type=int, default=None)
    parser.add_argument("--sample", help="process only a random sample"
                                         " of <SAMPLE> lexemes (useful for debugging)",
                        type=int, default=None)
    parser.add_argument("--debug", help="debug mode (changes logging level)",
                        action="store_true")
    parser.add_argument("-v", "--verbose", help="Verbose output writes a markdown rendering of alignments on stdout.",
                        action="store_true")

    args = parser.parse_args()

    log_level = logging.DEBUG if args.debug else logging.VERBOSE if args.verbose else logging.INFO
    logging.basicConfig(format='%(levelname)s:%(message)s', level=log_level)
    log = logging.getLogger()
    log.info(args)
    paradigms = pd.read_csv(args.paradigms, index_col=None, na_values="#DEF#")
    paradigms = paradigms.dropna(subset=["cell", "phon_form"], how="any")
    paradigms.reset_index(inplace=True)  # ensure unique numerical ID

    user_features = args.features is not None and args.features[-4:] == ".csv"

    method = []
    letters = sorted(paradigms["phon_form"].str.split(" ").explode().unique())
    int2alpha, alpha2int = scoring.init_alphabet(letters)

    if args.scores == "simple":
        method.append("Simple edit score")
        score_matrix = scoring.edit_matrix(len(int2alpha))
    else:
        if args.features == "bipa":
            method.append("CLTS features")
        else:
            method.append("User features")

        features = SoundInventory(int2alpha, args.features)
        if args.scores in ["frisch", "features"]:
            method.append(args.scores + " similarity")
            score_matrix = scoring.sim_matrix(features,
                                              int2alpha,
                                              alpha2int,
                                              method=args.scores)
        else:
            raise NotImplementedError()

    if args.algorithm == "Progressive":
        method.append("Progressive alignment")
        algorithm = align_multi.ProgressiveParadigmAligner(int2alpha, alpha2int, score_matrix)
    elif args.algorithm == "LCSProgressive":
        method.append("Progressive alignment with LCS")
        algorithm = align_multi.LCSProgressiveParadigmAligner(int2alpha, alpha2int, score_matrix)
    else:
        raise NotImplementedError("Algorithm {} is not implemented".format(args.algoritm))

    morpho_aligner = align_multi.LexiconAligner(paradigms, algorithm, cpu_counts=args.cpu, sample=args.sample)
    paradigms = morpho_aligner.align()
    paradigms["method"] = ", ".join(method)
    paradigms.to_csv(args.output, index=False)


def export_command():
    """Export html files of alignments."""
    import argparse

    usage = export_command.__doc__
    parser = argparse.ArgumentParser(description=usage,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("file",
                        help="aligned paradigm file, full path -- long shape, csv",
                        type=Path)
    parser.add_argument("output",
                        help="output directory path (for comp) or file path (for single file)",
                        type=Path)
    parser.add_argument("--comp",
                        help="compare with a second aligned paradigm file, full path -- long shape, csv",
                        type=Path, default=None)
    parser.add_argument("--lexemes",
                        help="Generate html files for specific lexemes",
                        type=list,
                        nargs="+",
                        )
    parser.add_argument("--standalone",
                        help="whether to generate a standalone page "
                             "(the alternative is to write the css"
                             " and the container directly)",
                        action="store_true",
                        )
    args = parser.parse_args()

    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    log = logging.getLogger()

    if args.standalone:
        html_template = "<h1>{lang} word {lexeme} ({pos})</h1>\n" \
                        "<div class='examples' id='{id}'>"
        html_structure = "<html><head><style>{styles}</style></head><body>{content}</body></html>"
    else:
        html_template = "<div class='examples' id='{id}'>"
        html_structure = "<style>{styles}</style>\n\n{content}"

    if args.lexemes is not None:
        args.lexemes = ["".join(l) for l in args.lexemes]

    if args.comp:
        args.output.mkdir(exist_ok=True)
        html_export.html_comparison(html_template, html_structure, args)


    else:
        meta_1, t_1, df_1 = html_export.read_alignments(args.file)
        colors = html_export.cell_colors(meta_1["vocabulary"] - {"-"})
        styles = html_export.STYLES + "\n".join(colors.values())
        log.info("Writing html file in %s", args.output)

        args.output.parent.mkdir(exist_ok=True)
        lg_id = meta_1["language_ID"] if "language_ID" in meta_1 else args.file.name
        lines = []

        if args.lexemes is None:
            args.lexemes = list(t_1)

        for l in tqdm(args.lexemes):
            lines.append("<h1>{} word {}</h1>".format(lg_id, l))
            lines.extend(html_export.html_alignment(t_1, df_1, l, meta_1))

        with args.output.open("w", encoding="utf-8") as f:
            f.write(html_structure.format(styles=styles, content="\n".join(lines)))
