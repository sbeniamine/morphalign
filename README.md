![coverage](https://gitlab.com/sbeniamine/morphalign/badges/master/pipeline.svg?job=test)

Aligns segments in inflectional paradigms.
The package is still experimental and under heavy development.

Task: Given paradigms, align the segments in forms of each lexeme together, in order to notice what changed and what is repeated across forms.

Supra-segmentals are not part of alignments, as their alignment is implicit from the alignment of the segmental material. 
Writing them punctually in the sequence of phonemes is a mere convention. Further processing steps are expected to produce a more complex
representation, where supra segmentals are mapped to segmental positions.

# Specific versions

The code on master should be the most stable and up to date.

Version  [v0.1.1-scil](https://gitlab.com/sbeniamine/morphalign/-/tree/v0.1.1-scil) of the code was used for the paper:

> Beniamine, Sacha and Guzmán Naranjo, Matías (2021) "[Multiple alignments of inflectional paradigms](https://scholarworks.umass.edu/scil/vol4/iss1/21)" *Proceedings of the Society for Computation in Linguistics*: Vol. 4 , Article 21,

# Install

You can install in development mode with pip:

~~~
cd morphalign/
pip install -e ./
~~~

# Input format

Minimally, this software requires the `paradigms` table from a [Paralex lexicon](https://www.paralex-standard.org).
To provide user-specified sound definitions, the `sounds` file is also necessary.
User-specified sound definitions make it possible to ensure a tiered approach, by defining supra-segmental and segmental tiers.

## Paradigms

csv file, in long format. The columns must include:

- `lexeme``: Any lexeme identifier, be it a number, a lemma, etc. Often this is the orthographic form.
- `cell``: paradigm form. This is also only an identifier, and will not be decomposed, though using meaningful labels is useful for readability.
- `phon_form``: space separated sequence of phonemes. Each phoneme should be defined in the phonological segments file. 
 If using user-specified lists of sounds, supra-segmentals are expected to be represented as diacritics or characters 
 written on the phoneme they affect or the nucleus of the syllable they affect. 
 Eg. /p i ʔ n iː ʧʰ oː˦ ʃ/ for the navajo verb bi'niichóósh (ipfv.2.sg, to eat). 
 If using clts defined sounds, supra-segmentals won't be autodetected, except for tones which should constitute their own separate phonemes, eg.
 /p i ʔ n iː ʧʰ oː ˦ ʃ/.

Example for a few forms of French verbs:

~~~
lexeme,cell,phon_form
forfaire,prs.1sg,f O ʁ f E
gésir,prs.1sg,ʒ i
surfaire,prs.1sg,s y ʁ f E
ahaner,prs.1sg,a a n
abâtardir,prs.1sg,a b a t a ʁ d i
abattre,prs.1sg,a b a
~~~

## Phonological segments

The sounds file is expected to have columns:

- `sound_id` (with the sound itself as written in the paradigms),
- `tier`: either one of "segmental", "tone", "length", or a custom string.
- `value`: an alternate, non diacritic notation for supra-segmentals when represented on their own. This may be identical to `sound_id`.
- Any number of columns representing a decomposition of the segmental sounds into distinctive features.
 `0` means -FEATURE; `1` means +FEATURE; a blank cell or `-1` mean that the feature is not relevant for this sound.
 These are used (and need to be specified) only for segmental sounds, and not for suprasegmentals.

This file can be customized to slice the data into tiers.

I recommend to use mono-valent features (1 or nothing, not 1, 0 or nothing) as much as possible, 
especially for the natural class similarity measure.

An alternative is to use CLTS' BIPA, when using standard BIPA sounds.
 
# Usage

## For finding alignments:

~~~
usage: morphalign [-h] [-f FEATURES] [-a {Progressive,LCSProgressive}] 
    [-s {simple,frisch,features}] [--cpu CPU] [--sample SAMPLE] [--debug] [-v] paradigms output


~~~
Example: 

~~~
morphalign -f <sounds_file> -cpu 7 -s features -o aligned_paradigms.csv -- paradigms.csv
~~~

The algorithm (`-a`) is either `Progressive` or `LCSProgressive`. 
The scoring scheme can be based on identity (`simple`, does not require features),
phonological similarity based on features directly (`features`) or based on
natural classes following Frisch 1997 (`frisch`).
The latter works much better with very sparse features, or ideally monovalent features.
It is possible to either use a single cpu thread, or several. The number is given to `--cpu`.
The `output` path must also be specified.

The output will have added columns:

- alignment: list of column indices. These are co-indexed with the phonemes in the form.
- stem:	constant non-discriminative (segmental) stem, useful for a cursory glance at the result
- exponents: entire (segmental) word minus the constant stem, useful for a cursory glance at the result
- method: alignment method used to create this file

The stem & exponents columns are only a very quick segmentation, meant to give an intuition about alignment quality for visual inspection.

## For evaluating alignments:

~~~
morphalign-eval [-h] [-o OUTPUT] [--cpu CPU] paradigms
~~~

Provide either a result file with the paradigms which were given as output of `align_paradigms.py`,
or a directory with multiple result files.

## For exporting html tables

~~~
morphalign-export [-h] [--comp COMP] [--lexemes LEXEMES [LEXEMES ...]] file output
~~~

There are two expected usage, depending on whether you want a selection of lexemes from a single file,
 or a comparison of differences between two files (in order to compare results obtained on the same data using different methods).

Example for exporting a few lexemes in a single html file:

~~~
morphalign-export --lexemes manger chanter dormir -- paradigmes_fr.csv alignements_fr.html
~~~

Examples for comparing two files:

~~~
morphalign-export --comp paradigmes_fr_method2.csv -- paradigmes_fr_method1.csv comparisons_fr/
~~~

This will export one html file for each lexeme which is analyzed differently by the two methods, in the output directory.